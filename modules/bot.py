import ChatBot
import text_parser
from flask import request
import search
import text_parser
chatbot = ChatBot.ChatBot()


def get_response(input_text=None):
    data = request.get_json()
    print(data)
    if "image" in data and len(data["image"]) > 0:
        image = data["image"]
        text = search.extract_text(image)
        print(text)
        info = search.search_beer_fact(text)
        print(info)
        return "info", info, {}
    
    input_text = input_text if input_text is not None else data["text"] if "text" in data else None
    
    response = chatbot.response(input_text)
    response_type, reply, extra = text_parser.parse_text(response)
    return response_type, reply, extra

