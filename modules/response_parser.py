def parse_have_beer(beer):
    return {
            "buttons": [{
                "text": "YES",
                "data": {
                    "command": "YES_HAVE_BEER",
                    "value": beer
                }
            },
                {
                    "text": "NO",
                    "data": {
                        "command": "NO_HAVE_BEER",
                        "value": beer
                    }
                }
            ]
        }


def parse_heading_out():
    return {
        "buttons": [{
            "text": "YES",
            "data": {
                "command": "YES_HEADING_OUT",
                "value": "YES_HEADING_OUT"
            }
        },
            {
                "text": "NO",
                "data": {
                    "command": "NO_HEADING_OUT",
                    "value": "NO_HEADING_OUT"
                }
            }
        ]
    }


def parse_see_promos():
    return {
        "buttons": [{
            "text": "YES",
            "data": {
                "command": "YES_PROMOS",
                "value": "YES_PROMOS"
            }
        },
            {
                "text": "NO",
                "data": {
                    "command": "NO_PROMOS",
                    "value": "NO_PROMOS"
                }
            }
        ]
    }


def parse_find_bars(bars):
    return {
        "buttons": [{
            "text": bar["_source"]["POCName"],
            "data": {
                "command": "GET_BAR",
                "value": bar["_id"]
            }
        } for bar in bars]
    }


def parse_feelings():
    feelings = ["Happy", "Crap", "Thirsty"]
    return {
        "buttons": [{
            "text": feel,
            "data": {
                "command": "FEELING_OPTION",
                "value": feel + "_Feeling"
            }
        } for feel in feelings]
    }


def parse_how_much():
    return {
        "buttons": [{
            "text": "RESPONSIBLE",
            "data": {
                "command": "ONE_DRINK",
                "value": "ONE_DRINK"
            }
        },
            {
                "text": "I just got fired",
                "data": {
                    "command": "TWO_DRINK",
                    "value": "TWO_DRINK"
                }
            }
        ]
    }


def parse_find_friends():
    return {
        "buttons": [{
            "text": "Duong",
            "data": {
                "command": "FIND_FRIEND",
                "value": "FIND_FRIEND"
            }
        },
            {
                "text": "Ben",
                "data": {
                    "command": "FIND_FRIEND",
                    "value": "FIND_FRIEND"
                }
            },
            {
                "text": "Sarah",
                "data": {
                    "command": "FIND_FRIEND",
                    "value": "FIND_FRIEND"
                }
            },
            {
                "text": "Roland",
                "data": {
                    "command": "FIND_FRIEND",
                    "value": "FIND_FRIEND"
                }
            }
        ]
    }


def mia():
    return {
        "buttons": [{
            "text": "Cancel",
            "data": {
                "command": "MIA",
                "value": "MIA"
            }
        }]
    }