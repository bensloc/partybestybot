from elasticsearch import Elasticsearch
import requests
import json
es = Elasticsearch("https://search-partybot-ot5mp5oral65qd2n6z35yorb5u.ap-southeast-2.es.amazonaws.com")


def find_bars(lon, lat):
    query = {
        "query": {
            "bool" : {
                "must" : {
                    "match_all" : {}
                },
                "filter" : {
                    "geo_distance" : {
                        "distance" : "10km",
                        "Location" : "{},{}".format(lat, lon)
                        }
                    }
                }
            }
        }
    
    results = es.search(index="beer", doc_type="data", body=query, size=5)["hits"]["hits"]
    return results


def get_bar(id):
    return es.get(index="beer", doc_type="data", id=id)


def extract_text(image):
    data = {
        "requests": [{
            "image": {
                "content": image
            },
            "features": {
                "type": "TEXT_DETECTION",
                "maxResults": 10
            }
        }]
    }
    params = {
        "key": "AIzaSyCr3DoaEVu2HhRQQBocR2WcXrnvixRrzNU"
    }
    url = "https://vision.googleapis.com/v1/images:annotate"
    resp = requests.post(url=url, params=params, data=json.dumps(data))
    data = json.loads(resp.text)
    if len(data["responses"]) > 0:
        resp = data["responses"][0]
        if "textAnnotations" in resp:
            return resp["textAnnotations"][0]["description"]
    else:
        return None


def search_beer_fact(text):
    if text is None:
        return ""
    text = text.encode("ascii", "ignore").decode("utf-8", "ignore")
    text = text.replace("\n", " ")
    text = " ".join([w for w in text.split(" ") if w.isalnum()])
    print(text)
    query = {
        "query": {
            "query_string": {
                "default_field": "Brand",
                "query": text
            }
        }
    }

    results = es.search(index="nutrition", doc_type="data", body=query)["hits"]["hits"]
    if len(results) > 0:
        return results[0]["_source"]
    else:
        return None