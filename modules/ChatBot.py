
import jpype
import os

class ChatBot:
   
    BotClass = None
    ChatClass = None
    botName = "alice2"
    chat = None
    bot = None
    
    @staticmethod
    def loadConfig(botName=None, path=None):
        
        if ChatBot.BotClass is None:
            ChatBot.BotClass = jpype.JPackage("org").alicebot.ab.Bot
        if ChatBot.ChatClass is None:
            ChatBot.ChatClass = jpype.JPackage("org").alicebot.ab.Chat
        if botName is None:
            if path is None:
                ChatBot.bot = ChatBot.BotClass(ChatBot.botName)
            else:
                if not os.path.exists(path + "/bots"):
                    raise Exception("Can not load bot")
                ChatBot.bot = ChatBot.BotClass(ChatBot.botName, path)
        else:
            if path is None:
                ChatBot.bot = ChatBot.BotClass(botName)
            else:
                if not os.path.exists(path + "/bots"):
                    raise Exception("Can not load bot")
                ChatBot.bot = ChatBot.BotClass(botName, path)
        ChatBot.chat = ChatBot.ChatClass(ChatBot.bot)
    
    @staticmethod
    def response(sentence):

        if ChatBot.bot is None:
            dir = os.path.dirname(os.path.realpath(__file__))
            if os.path.exists(dir + "/bots"):
                ChatBot.loadConfig(None, dir)
            else:
                dir = os.getcwd()
                if os.path.exists(dir + "/bots"):
                    ChatBot.loadConfig(None, dir)
                else:
                    raise Exception("Can not load bot")
        return ChatBot.chat.multisentenceRespond(sentence)
