from flask import request
import search
import bot
import boto3
import json


aws_access_key_id="AKIAJ4VYW276B6BI62WA"
aws_secret_access_key="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"

lambda_client = boto3.client("lambda",
                             "ap-southeast-2",
                             aws_access_key_id=aws_access_key_id,
                             aws_secret_access_key=aws_secret_access_key)


def parse_command():
    args = request.get_json()
    command = args["command"]
    if command == "GET_BAR":
        return parse_get_bar()
    elif "HAVE_BEER" in command:
        print(command)
        result = parse_have_beer()
        print(result)
        return result
    elif "feeling" in command.lower():
        return parse_feeling()
    elif "heading_out" in command.lower():
        return parse_heading_out()
    elif "promos" in command.lower():
        return parse_see_promos()
    elif "friend" in command.lower():
        return parse_find_friend()
    elif "drink" in command.lower():
        args = request.get_json()
        command = args['command']
        value = args["value"]
        response_type, reply, extra = bot.get_response(value)
        return response_type, reply, extra
    else:
        return None, None, {}
    

def parse_get_bar():
    args = request.get_json()
    id = args["value"]
    bar = search.get_bar(id)['_source']
    location = bar["Location"].split(",")
    bar["google_map"] = "https://www.google.com/maps/search/?api=1&query=%s,%s" % (location[0], location[1])
    return "info", "Here is the information", {"bar": bar}


def parse_have_beer():
    args = request.get_json()
    command = args['command']
    value = args["value"]
    response_type = None
    reply = None
    extra = {}
    if "NO" in command:
        # response_type, reply, extra = bot.get_response("TAKE PHOTO")
        response_type, reply, extra = bot.get_response("ADD BEER")
    elif "YES" in command:
        return "add_drink", "Cool! I'll add it to your tracker.", {"beer": value}
        
    return response_type, reply, extra


def parse_feeling():
    args = request.get_json()
    command = args['command']
    value = args["value"]
    response_type, reply, extra = bot.get_response(value)
    return response_type, reply, extra


def parse_heading_out():
    args = request.get_json()
    command = args['command']
    value = args["value"]
    response_type, reply, extra = bot.get_response(value)
    return response_type, reply, extra


def parse_see_promos():
    args = request.get_json()
    command = args['command']
    value = args["value"]
    response_type, reply, extra = bot.get_response(value)
    return response_type, reply, extra


def parse_find_friend():
    phone = "+610404747346"
    message = "It's Charlie. Come and have a drink with me at EIGHT ACRES"
    resp = lambda_client.invoke(
        FunctionName="send_sms",
        Payload=json.dumps({
            "phone": phone,
            "message": message
        }))
    return "text", "Duong has been invited", {}
