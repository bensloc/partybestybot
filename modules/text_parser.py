import random
import response_parser
import search
from flask import request


def split_command_response(text):
    splits = text.split("|")
    command = None
    response = None
    if len(splits) == 2:
        command = splits[0]
        response = splits[1]
    else:
        response = text
    
    return command, response
    

def parse_text(text):
    command, response = split_command_response(text)
    response_type, reply, extra = parse_command(command, response)
    return response_type, reply, extra


def parse_command(command, text):
    response_type = None
    reply = None
    extra = {}
    if command == "HAVE_BEER":
        response_type, reply, extra = parse_having_beer(command, text)
    elif command == "FIND_BAR":
        response_type, reply, extra = parse_find_bars(command, text)
    elif command == "FEELING_OPTIONS":
        response_type, reply, extra = parse_feeling(command, text)
    elif command == "HEADING_OUT":
        response_type, reply, extra = parse_heading(command, text)
    elif command and "promos" in command.lower():
        response_type, reply, extra = parse_see_promos(command, text)
    elif command == "HOW_MUCH":
        reply = text
        response_type = "options"
        extra = response_parser.parse_how_much()
    elif command == "FIND_FRIEND":
        reply = text
        response_type = "options"
        extra = response_parser.parse_find_friends()
    elif command == "CALL_MIA":
        reply = text
        response_type = "options"
        extra = response_parser.mia()
    else:
        reply = text
        response_type = "text"
    return response_type, reply, extra


def parse_having_beer(command, text):
    beer_opts = ["Pure Blond", "Stella", "Corona"]
    beer = random.choice(beer_opts)
    response = text.format(beer)
    
    return "options", response, response_parser.parse_have_beer(beer)


def parse_find_bars(command, text):
    args = request.get_json()
    print(args)
    lon = float(args["longitude"])
    lat = float(args["latitude"])
    
    bars = search.find_bars(lon, lat)
    
    return "options", text, response_parser.parse_find_bars(bars)


def parse_feeling(command, text):
    return "options", text, response_parser.parse_feelings()


def parse_heading(command, text):
    return "options", text, response_parser.parse_heading_out()


def parse_see_promos(command, text):
    return "options", text, response_parser.parse_see_promos()
