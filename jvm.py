import platform, os, jpype


def getJavaPath():
   
    # check if the JAVA_HOME exists in the environment
    JVM_PATH = None
    if 'JAVA_HOME' in os.environ:
        JVM_PATH = os.environ['JAVA_HOME'].rstrip("/").rstrip("\\")
    else:
        # check if the JDK_HOME exists in the environment
        if 'JDK_HOME' in os.environ:
            JVM_PATH = os.environ['JDK_HOME'].rstrip("/").rstrip("\\")
        else:
            # raise exception if both do not exist
            raise (exc.JavaPathNotFoundException())
    return JVM_PATH


def getOSJavaClient():
    '''
    Get the java client library based on the Operating system 
    This checks if the current OS is either Windows or Linux. If neither of them is found, it will raise the OSNotSupportedException
    :return java client: library path
    :raise OSNotSupportedException: only Linux and Windows  
    '''
    client_path = None
    OS_NAME = platform.system()
    if OS_NAME == "Linux":
        bit_version = platform.architecture()
        if bit_version[0] == "32bit":
            client_path = "/jre/lib/i386/client/libjvm.so"
        else:
            client_path = "/jre/lib/amd64/client/libjvm.so"
    else:
        if OS_NAME == "Windows":
            client_path = "/jre/bin/client/jvm.dll"
        elif OS_NAME == "Darwin":
            # OS is not supported
            client_path = "/jre/lib/server/libjvm.dylib"
    return client_path


def startJVM(lib_path):
    jvm_path = None
    print(lib_path)
    try:
        jvm_path = getJavaPath() + getOSJavaClient()
    except Exception as e:
        raise e
        
        # start JVM
    try:
        # print jvm_path
        jpype.startJVM(jvm_path, "-Djava.ext.dirs={}".format(lib_path))
    except Exception as e:
        raise e


def stopJVM():
    '''
     Stop the jvm
    '''
    jpype.shutdownJVM()
