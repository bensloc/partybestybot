from flask import Flask
from modules import bot
import jvm
import os
from flask import request
from flask import jsonify
import jpype
import boto3
import json
from flask_cors import CORS
from flask import Response
from modules import command


cwd = os.path.abspath(os.path.dirname(__file__))
jvm.startJVM(cwd + "/modules/lib")
app = Flask(__name__)
CORS(app)

aws_access_key_id="AKIAJ4VYW276B6BI62WA"
aws_secret_access_key="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"

lambda_client = boto3.client("lambda",
                             "ap-southeast-2",
                             aws_access_key_id=aws_access_key_id,
                             aws_secret_access_key=aws_secret_access_key)


@app.route("/chat", methods=["POST"])
def get_response():
    jpype.attachThreadToJVM()
    headers = {
        "Content-Type": "application/json"
    }
    response_type, reply, extra = bot.get_response()
    body = {
        "reply": reply,
        "type": response_type
    }
    body.update(extra)
    resp = Response(json.dumps(body), headers=headers)
    print(resp.response)
    return resp


@app.route("/chat/command", methods=["POST"])
def process_command():
    jpype.attachThreadToJVM()
    print(request.get_json())
    headers = {
        "Content-Type": "application/json"
    }
    response_type, reply, extra = command.parse_command()
    body = {
        "reply": reply,
        "type": response_type
    }
    body.update(extra)
    resp = Response(json.dumps(body), headers=headers)
    return resp


@app.route("/alert", methods=["POST"])
def alert():
    jpype.attachThreadToJVM()
    data = request.get_json(force=True)
    phone = data["phone"]
    message = data["message"]
    resp = lambda_client.invoke(
        FunctionName="send_sms",
        Payload=json.dumps({
            "phone": phone,
            "message": message
        }))
    print(resp)
    
    return jsonify(status="ok")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9000, debug=True)